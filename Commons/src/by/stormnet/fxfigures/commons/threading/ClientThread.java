package by.stormnet.fxfigures.commons.threading;

import by.stormnet.fxfigures.commons.geometry.Figure;
import by.stormnet.fxfigures.commons.messaging.Message;
import by.stormnet.fxfigures.commons.messaging.MessageDispatcher;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.SocketException;

public class ClientThread extends Thread implements IClientThread {
    private Socket socket;
    private ObjectOutputStream oos;
    private ObjectInputStream ois;

    public ClientThread(Socket socket) throws IOException {
        oos = new ObjectOutputStream(socket.getOutputStream());
        ois = new ObjectInputStream(socket.getInputStream());
        this.socket = socket;
    }

    private MessageDispatcher messageDispatcher;
    private boolean isRunning;

    @Override
    public void setClientThreadListener(MessageDispatcher listener) {
        this.messageDispatcher = listener;
    }

    @Override
    public <M extends Message> void sendMessage(M message) throws IOException {
        if(oos !=null){
            synchronized (ClientThread.class) {
                oos.writeObject(message);
            }
        }

    }

    @Override
    public void stopThread() {
        isRunning = false;
    }


    @Override
    public void run() {
        isRunning = true;
        while(isRunning){
            try {
                Message<Figure> msg = (Message<Figure>) ois.readObject();
                if (msg != null & messageDispatcher != null) {
                    messageDispatcher.dispatchMessage(msg);
                }
                Thread.sleep(100L);
            } catch(SocketException e){
                e.printStackTrace();
                if(messageDispatcher != null){
                    messageDispatcher.unregisterClienThread(this);
                }

            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
                isRunning = false;
            }

        }

        try {
            oos.close();
            ois.close();
            socket.close();
            System.out.println("Поток клиента умер");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
