package by.stormnet.fxfigures.commons.Phisics;

import by.stormnet.fxfigures.commons.geometry.Circle;
import by.stormnet.fxfigures.commons.geometry.Rectangle;
import by.stormnet.fxfigures.commons.geometry.Triangle;

import java.util.LinkedList;
import java.util.List;

public class PhysicsWorld extends Thread{
    private double worldWidht;
    private double worldHeight;
    private boolean isRunning;
    private long lastUpdateTime;

    private List<Body> bodies;
    private WorldCallbackListener listener;

    public PhysicsWorld(String name, double worldWidht, double worldHeight) {
        super(name);
        this.worldWidht = worldWidht;
        this.worldHeight = worldHeight;
        bodies = new LinkedList<>();
    }

    public void registerCallbackListener(WorldCallbackListener listener){
        this.listener = listener;
    }

    public Body createBody(){
        Body body = new Body();
        bodies.add(body);
        return body;
    }

    @Override
    public void run() {
        isRunning = true;
        lastUpdateTime = System.currentTimeMillis();
        while (isRunning){
            checkOuts();
            bodies.forEach(body -> {
                body.move(System.currentTimeMillis() - lastUpdateTime);
            });
            if(listener != null)
                listener.callback();
            lastUpdateTime = System.currentTimeMillis();
            try {
                Thread.sleep(15L);
            } catch (InterruptedException e) {
                e.printStackTrace();
                isRunning = false;
            }
        }
        bodies.clear();
        System.out.println("Поток фигур умер");

    }

    public void finish(){
        isRunning = false;

    }

    public double getWorldWidht() {
        return worldWidht;
    }

    public double getWorldHeight() {
        return worldHeight;
    }

    private void checkOuts() {
        bodies.forEach(body -> {
            if(body.getFigure().getType() == 0) {
                if (body.getFigure().getcX() + ((Circle) body.getFigure()).getRadius() >= worldWidht ||
                        body.getFigure().getcX() - ((Circle) body.getFigure()).getRadius() <= 0) {
                    body.setVx(body.getVx() * -1);
                }
                if (body.getFigure().getcY() + ((Circle) body.getFigure()).getRadius() >= worldHeight ||
                        body.getFigure().getcY() - ((Circle) body.getFigure()).getRadius() <= 0) {
                    body.setVy(body.getVy() * -1);
                }
            }
            if(body.getFigure().getType() == 1) {
                if (body.getFigure().getcX() + ((Rectangle) body.getFigure()).getWidth() >= worldWidht ||
                        body.getFigure().getcX() - ((Rectangle) body.getFigure()).getWidth() <= 0) {
                    body.setVx(body.getVx() * -1);
                }
                if (body.getFigure().getcY() + ((Rectangle) body.getFigure()).getHeight() >= worldHeight ||
                        body.getFigure().getcY() - ((Rectangle) body.getFigure()).getHeight() <= 0) {
                    body.setVy(body.getVy() * -1);
                }
            }
            if(body.getFigure().getType() == 2){
                if(body.getFigure().getcX() + ((Triangle)body.getFigure()).getBase() >= worldWidht ||
                        body.getFigure().getcX() - ((Triangle)body.getFigure()).getBase() <= 0){
                    body.setVx(body.getVx() * -1);
                }
                if(body.getFigure().getcY() + ((Triangle)body.getFigure()).getBase() >= worldHeight ||
                        body.getFigure().getcY() - ((Triangle)body.getFigure()).getBase() <= 0){
                    body.setVy(body.getVy() * -1);
                }
            }
        });

    }
}
