package by.stormnet.fxfigures.commons.Phisics;

import by.stormnet.fxfigures.commons.geometry.Figure;

public class Body {
    private double vx;
    private double vy;

    private Figure figure;

    public Body() {
    }

    public void move (long deltaTime){
        double sx = vx * deltaTime;
        double sy = vy * deltaTime;
        figure.setcX(figure.getcX() + sx);
        figure.setcY(figure.getcY() + sy);
    }

    public double getVx() {
        return vx;
    }

    public void setVx(double vx) {
        this.vx = vx;
    }

    public double getVy() {
        return vy;
    }

    public void setVy(double vy) {
        this.vy = vy;
    }

    public Figure getFigure() {
        return figure;
    }

    public void setFigures(Figure figures) {
        this.figure = figures;
    }
}
