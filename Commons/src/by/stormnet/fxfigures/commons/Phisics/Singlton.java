package by.stormnet.fxfigures.commons.Phisics;


public class Singlton {
    private static volatile Singlton instanse = null;
    private PhysicsWorld physicsWorld;

    public Singlton() {
    }

    public static Singlton getInstanse(){
        if (instanse == null)
            synchronized (Singlton.class){
                if(instanse == null)
                    instanse = new Singlton();


            }
        return instanse;

    }

    public PhysicsWorld getPhysicsWorld() {
        return physicsWorld;
    }

    public void setPhysicsWorld(PhysicsWorld physicsWorld) {
        this.physicsWorld = physicsWorld;
    }
}
