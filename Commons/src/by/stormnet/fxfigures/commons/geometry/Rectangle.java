package by.stormnet.fxfigures.commons.geometry;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class Rectangle extends Figure {
    private double width;
    private double height;

    public Rectangle(double cX, double cY, double lineWidth, Color color, double width, double height) {
        this(cX, cY, lineWidth, color);
        this.width = width;
        this.height = height;
    }

    private Rectangle(double cX, double cY, double lineWidth, Color color) {
        super(FIGURE_TYPE_RECTANGLE, cX, cY, lineWidth, color);
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    @Override
    public void draw(GraphicsContext gc) {
        gc.setStroke(color);
        gc.setLineWidth(lineWidth);
        gc.strokeRect(cX - width/2, cY - height/2, width, height);

    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Rectangle{");
        sb.append("width=").append(width);
        sb.append(", height=").append(height);
        sb.append(", cX=").append(cX);
        sb.append(", cY=").append(cY);
        sb.append(", lineWidth=").append(lineWidth);
        sb.append(", color=").append(color);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public void printInfo() {
        System.out.println(toString());
    }

    @Override
    public double printSquare() {
        return getHeight()*getWidth();
    }
}
