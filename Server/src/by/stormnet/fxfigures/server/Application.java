package by.stormnet.fxfigures.server;

import java.io.IOException;
import java.util.Scanner;

public class Application {
    private Scanner scanner;
    private boolean serverStarted;

    public static void main(String[] arg) {
        new Application().runTheApp();
    }

    private void runTheApp() {
        while(!serverStarted){
            try {
                Server server = new Server(readTheUserInput("ведите номер порта (значение от 1025 до 65535):"));
                server.start();
                serverStarted = true;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private int readTheUserInput(String string){
        System.out.println(string);
        scanner = new Scanner(System.in);
        if(scanner.hasNextInt()){
            return scanner.nextInt();
        }
        System.out.println("Wrong value, try again!");
        return readTheUserInput(string);
    }
}
