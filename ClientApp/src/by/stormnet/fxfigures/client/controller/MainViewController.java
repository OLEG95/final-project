package by.stormnet.fxfigures.client.controller;

import by.stormnet.fxfigures.client.ClientApp;
import by.stormnet.fxfigures.commons.Phisics.Body;
import by.stormnet.fxfigures.commons.Phisics.PhysicsWorld;
import by.stormnet.fxfigures.commons.Phisics.Singlton;
import by.stormnet.fxfigures.commons.Phisics.WorldCallbackListener;
import by.stormnet.fxfigures.commons.geometry.*;
import by.stormnet.fxfigures.commons.messaging.Message;
import by.stormnet.fxfigures.commons.messaging.MessageDispatcher;
import by.stormnet.fxfigures.commons.threading.ClientThread;
import by.stormnet.fxfigures.commons.threading.IClientThread;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;


import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.URL;
import java.util.*;

public class MainViewController<T extends Thread & IClientThread> implements Initializable, MessageDispatcher<T>, WorldCallbackListener {
    private LinkedList<Figure> figures;
    private Random rnd;
    private ClientThread clientThread;
    private PhysicsWorld world;

    @FXML
    private Canvas canvas;

    public MainViewController() {
        figures = new LinkedList<>();
        rnd = new Random(System.currentTimeMillis());
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {
            Socket socket = new Socket(InetAddress.getByName("localhost"), 9999);
            clientThread = new ClientThread(socket);
            registerClientThread(null);
        } catch (IOException e) {
            e.printStackTrace();
        }



    }


    @FXML
    private void onMouseClicked(MouseEvent event) {
//        System.out.println("x: " + event.getX());
//        System.out.println("y: " + event.getY());
        Message<Figure> message = new Message<>(createFigure(event.getX(), event.getY()));
        if (clientThread != null){
            try {
                clientThread.sendMessage(message);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        world = new PhysicsWorld("World", ClientApp.SCREEN_WIDTH, ClientApp.SCREEN_HEIGHT);
        world.registerCallbackListener(this);
        world.start();
        Singlton.getInstanse().setPhysicsWorld(world);




    }


    private Figure createFigure(double x, double y){
        Figure figure = null;
        int type = rnd.nextInt(3);
        switch(type){
            case Figure.FIGURE_TYPE_CIRCLE:
                System.out.println("create circle");
                double lineWidthCircle = rnd.nextInt(6);
                double radius = rnd.nextInt(101);
                figure = new Circle(x, y, lineWidthCircle == 0 ? 1 : lineWidthCircle, Color.RED,
                        radius < 10 ? 10 : radius);
                break;
            case Figure.FIGURE_TYPE_RECTANGLE:
                System.out.println("create rectangle");
                double lineWidthRectangle = rnd.nextInt(6);
                double width = rnd.nextInt(101);
                double height = rnd.nextInt(101);
                figure = new Rectangle(x, y, lineWidthRectangle == 0 ? 1 : lineWidthRectangle, Color.CORNFLOWERBLUE,
                        width < 10 ? 10 : width, height < 10 ? 10 : height );
                break;
            case Figure.FIGURE_TYPE_TRIANGLE:
                System.out.println("create triangle");
                double lineWidthTriangle = rnd.nextInt(6);
                double base = rnd.nextInt(101);
                figure = new Triangle(x, y, lineWidthTriangle == 0 ? 1 : lineWidthTriangle, Color.LIME,
                        base < 10 ? 10 : base);
                break;
            default:
                System.out.println("Unknown figure type.");

        }
        return figure;
    }

    private void repaint(){
        if(figures == null)
            return;
        canvas.getGraphicsContext2D().clearRect(0,0, ClientApp.SCREEN_WIDTH, ClientApp.SCREEN_HEIGHT);
        for (Figure figure : figures){
            if (figure != null){
                figure.draw(canvas.getGraphicsContext2D());
            }
        }
    }

    @Override
    public void registerClientThread(T client) {
        clientThread.setClientThreadListener(this);
        clientThread.start();

    }

    @Override
    public void unregisterClienThread(T client) {

    }

    @Override
    public <M extends Message> void dispatchMessage(M message) {
        Figure figure = message.getFigure();
        switch(figure.getType()){
            case Figure.FIGURE_TYPE_RECTANGLE:
                figure.setColor(Color.CORNFLOWERBLUE);
                break;
            case Figure.FIGURE_TYPE_CIRCLE:
                figure.setColor(Color.RED);
                break;
            case Figure.FIGURE_TYPE_TRIANGLE:
                figure.setColor(Color.LIME);
                break;
            default:
                System.out.println("Unknown figure type.");
        }
        figures.add(figure);
        Body body = world.createBody();
        body.setFigures(figure);
        body.setVx(0.5);
        body.setVy(0.25);

    }

    @Override
    public void clear() {
        clientThread.stopThread();
    }

    @Override
    public void callback() {
        Platform.runLater(this::repaint);
    }
}
